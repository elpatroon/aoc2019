#!/usr/bin/python

## MODULES
import sys
import math
import argparse
import numpy as np

## PARSE ARGUMENTS
parser = argparse.ArgumentParser(description="Define the input file for the puzzle!")
parser.add_argument("--input", default=None, required=True, type=str, help="Input file for puzzle!")
args = parser.parse_args()

## Initialize variables
with open(args.input,"r") as inputFile:
    code = inputFile.readlines()[0].split('-')

## CODE
partOne = 0
partTwo = 0

for passWord in range(int(code[0]), int(code[1])+1):
    intList = str(passWord)
    if len(np.unique(intList)) != len(intList):
        neverDecrease = True
        adjacentSame = False
        onlyTwo = False
        for i in range(len(intList) - 1):
                if intList[i] > intList[i+1]:
                    neverDecrease = False
                    break
                if intList[i] == intList[i+1]:
                    adjacentSame = True
                    if intList.count(intList[i]) == 2:
                        onlyTwo = True
        if neverDecrease == adjacentSame == True:
            partOne += 1
            if onlyTwo == True:
                partTwo += 1

print("The answer for part 1 is: " + str(partOne))
print("The answer for part 2 is: " + str(partTwo))
