#!/usr/bin/python

## MODULES
import sys
import math
import argparse

## PARSE ARGUMENTS
parser = argparse.ArgumentParser(description="Define the input file for the puzzle!")
parser.add_argument("--input", default=None, required=True, type=str, help="Input file for puzzle!")
args = parser.parse_args()
input = args.input

## Initialize variables
f= open(input,"r")

## CODE
totalfuel = 0
totalfuelwithfuelforfuel = 0

def fuel_to_add_for_weight(mass):
    return math.floor((int(mass) / 3)) - 2

def calculate_total_fuel_for_module(weight):
    totalfuelformodule = 0
    while weight > 0:
        weight = fuel_to_add_for_weight(weight)
        if weight > 0:
            totalfuelformodule += weight
    return totalfuelformodule

for module in f:
    totalfuel += fuel_to_add_for_weight(int(module))
    totalfuelwithfuelforfuel += calculate_total_fuel_for_module(int(module))

print("The answer for part 1 is: " + str(totalfuel))
print("The answer for part 2 is: " + str(totalfuelwithfuelforfuel))
