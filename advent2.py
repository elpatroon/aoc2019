#!/usr/bin/python

## MODULES
import sys
import math
import argparse

## PARSE ARGUMENTS
parser = argparse.ArgumentParser(description="Define the input file for the puzzle!")
parser.add_argument("--input", default=None, required=True, type=str, help="Input file for puzzle!")
args = parser.parse_args()
input = args.input

## INITIALIZE VARIABLES

intcode = []
with open(input, "r") as f:
    for a in f:
        split = a.split(",")
    for item in split:
       intcode.append(int(item))

##CODE

def program(noun, verb, intcode):
    DC = intcode.copy()
    DC[1] = noun
    DC[2] = verb

    for int in range(0, len(DC), 4):
        if DC[int] == 99:
            return DC[0]
            break
        if DC[int] == 1:
            DC[DC[int+3]] = DC[DC[int+1]] + DC[DC[int+2]]
        else:
            DC[DC[int+3]] = DC[DC[int+1]] * DC[DC[int+2]]


# Part 1:
print("The answer for part 1 is: " + str(program(12,2,intcode)))

# Part 2:
for noun in range(0,100):
    for verb in range(0,100):
        if program(noun, verb,intcode) == 19690720:
            print ("The answer for part 2 is: " + str(100 * noun + verb))
